package com.trienoir.emuparadiser.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.trienoir.emuparadiser.R;
import com.trienoir.emuparadiser.fragments.GameListFragment;
import com.trienoir.emuparadiser.utils.Constants;

/**
 * Created by mClinica - Chad on 10/4/2017.
 */

public class GameDownloadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_download);

        int gameSystemOrdinal = getIntent().getIntExtra(Constants.GAME_SYSTEM_BUNDLE_KEY, 0);

        addFragment(GameListFragment.newInstance(gameSystemOrdinal), "game_list", null);
    }

    public void addFragment(Fragment fragment, String tag, String backStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.activity_game_download_fragment, fragment, tag);
        if (backStack != null) ft.addToBackStack("BS");
        ft.commit();
    }

}
