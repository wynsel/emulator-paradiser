package com.trienoir.emuparadiser.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.trienoir.emuparadiser.R;
import com.trienoir.emuparadiser.utils.Constants;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton btnSNES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSNES = (AppCompatButton) findViewById(R.id.activity_main_snes_button);
        btnSNES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GameDownloadActivity.class);
                intent.putExtra(Constants.GAME_SYSTEM_BUNDLE_KEY, 0);
                startActivity(intent);
            }
        });

    }
}
