package com.trienoir.emuparadiser.interfaces;

/**
 * Created by TrieNoir on 06/10/2017.
 */

public interface ListSelectorInterface {
    void OnItemSelected(int position);
}
