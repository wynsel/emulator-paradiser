package com.trienoir.emuparadiser.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trienoir.emuparadiser.R;
import com.trienoir.emuparadiser.interfaces.ListSelectorInterface;
import com.trienoir.emuparadiser.models.Game;
import com.trienoir.emuparadiser.utils.GenericListAdapter;

/**
 * Created by mClinica - Chad on 10/4/2017.
 */

public class GamesListAdapter extends GenericListAdapter<Game> {

    private Context mContext;
    private ListSelectorInterface mListSelectorInterface;

    public GamesListAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    protected RecyclerView.ViewHolder setOnCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_games_list, parent, false));
    }

    @Override
    protected void setOnBindViewHolder(RecyclerView.ViewHolder holder, final int position, final Game item) {
        final ViewHolder v = (ViewHolder) holder;

        v.tvId.setText(String.valueOf(item.getId()));
        v.tvTitle.setText(item.getTitle());
        v.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListSelectorInterface.OnItemSelected(position);
            }
        });
    }

    public void setListSelector(ListSelectorInterface mListSelectorInterface) {
        this.mListSelectorInterface = mListSelectorInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId, tvTitle;

        public ViewHolder(View v) {
            super(v);

            tvId = (TextView) v.findViewById(R.id.item_games_list_id);
            tvTitle = (TextView) v.findViewById(R.id.item_games_list_title);
        }
    }
}
