package com.trienoir.emuparadiser.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.trienoir.emuparadiser.R;

public class YesNoDialog {
	
	public interface AlertDialogService {
	    void PositiveMethod(DialogInterface dialog, int id);
	    void NegativeMethod(DialogInterface dialog, int id);
	}
	
	public static void getConfirmDialog(Context mContext, String title, String msg, String positiveBtnCaption, String negativeBtnCaption, boolean isCancelable, final AlertDialogService target) {
		LayoutInflater li = LayoutInflater.from(mContext);
		View promptsView = li.inflate(R.layout.dialog_yes_no, null);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(promptsView).setCancelable(false).setPositiveButton(positiveBtnCaption, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                target.PositiveMethod(dialog, id);
            }
        }).setNegativeButton(negativeBtnCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                target.NegativeMethod(dialog, id);
            }
        });
        
        TextView tvTitle = (TextView) promptsView.findViewById(R.id.tv_dialog_title);
        TextView tvMessage = (TextView) promptsView.findViewById(R.id.tv_dialog_message);
        
        tvTitle.setText(title);
        tvMessage.setText(msg);

        final AlertDialog alert = builder.create();
//        alert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        alert.setCancelable(isCancelable);
        alert.show();
        if (isCancelable) {
            alert.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface arg0) {
                    	alert.dismiss();
                }
            });
        }
    }
}
