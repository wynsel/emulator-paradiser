package com.trienoir.emuparadiser.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.trienoir.emuparadiser.R;
import com.trienoir.emuparadiser.activities.GameDownloadActivity;
import com.trienoir.emuparadiser.adapters.GamesListAdapter;
import com.trienoir.emuparadiser.dialogs.YesNoDialog;
import com.trienoir.emuparadiser.enums.GameSystem;
import com.trienoir.emuparadiser.interfaces.ListSelectorInterface;
import com.trienoir.emuparadiser.models.Game;
import com.trienoir.emuparadiser.utils.Constants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by TrieNoir on 06/10/2017.
 */

public class GameListFragment extends Fragment {

    //https://www.emuparadise.me/roms/get-download.php?gid=152649&token=6b3d559f6a1d346707de2c3cbeceac30&mirror_available=true
    //https://www.emuparadise.me/roms/get-download.php?gid=152649&token=6b3d559f6a1d346707de2c3cbeceac30&mirror_available=true
    //https://www.emuparadise.me/roms/get-download.php?gid=152649&token=6b3d559f6a1d346707de2c3cbeceac30&mirror_available=true
    //https://www.emuparadise.me/roms/get-download.php?gid=152649&token=6b3d559f6a1d346707de2c3cbeceac30&mirror_available=true
    //https://www.emuparadise.me/roms/get-download.php?gid=152536&token=c8f22e24aee08137e3639f872e4201bc&mirror_available=true
    //http://198.16.64.50/happyxhJ1ACmlTrxJQpol71nBc/may/120/Super%20Nintendo%20Roms/Xak%20-%20The%20Art%20of%20Visual%20Stage%20%28Japan%29.zip

    //http://50.7.161.234/happyxhJ1ACmlTrxJQpol71nBc/Playstation2Europe/isos/A-Train%206%20(Europe)%20(En,Fr,De).7z
    //http://50.7.92.186/happyxhJ1ACmlTrxJQpol71nBc/Playstation2Europe/isos/AC-DC%20Live%20-%20Rock%20Band%20(Europe)%20(En,Fr,De,Es,It).7z
    //http://50.7.136.26/happyxhJ1ACmlTrxJQpol71nBc/Playstation2Europe/isos/Ace%20Lightning%20(Europe).7z
    //ftp://50.7.161.122/Playstation2Europe/isos/Ace%20Lightning%20(Europe).7z

    //PS2
    //ftp://50.7.161.74/Playstation2Europe/isos/Action%20Man%20A.T.O.M.%20-%20Alpha%20Teens%20on%20Machines%20(Europe)%20(En,Fr,De,Es,It,Nl,No,Da,Fi).7z
    //ftp://208.77.22.75/Playstation2Europe/isos/Action%20Man%20A.T.O.M.%20-%20Alpha%20Teens%20on%20Machines%20(Europe)%20(En,Fr,De,Es,It,Nl,No,Da,Fi).7z
    //ftp://75.126.73.27/Playstation2Asia/Zero%20-%20Akai%20Chou%20(Japan).7z
    //ftp://68.235.37.3/Playstation2/Zatch%20Bell!%20Mamodo%20Fury%20(USA).7z

    //SNES
    //ftp://75.126.73.49/may/120/Super%20Nintendo%20Roms/AAAHH%21%21%21%20Real%20Monsters%20%28Europe%29.zip
    //ftp://75.126.5.157/may/120/Super%20Nintendo%20Roms/ABC%20Monday%20Night%20Football%20%28USA%29.zip
    //ftp://75.126.5.157/may/120/Super%20Nintendo%20Roms/Xak%20-%20The%20Art%20of%20Visual%20Stage%20%28Japan%29.zip

    public static final String LOG = "GameListFragment";
    public static final String GAME_LIST_SYSTEM = "game_list_system";

    private RecyclerView rvGameList;
    private GamesListAdapter mGameListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private GameDownloadActivity mGameDownloadActivity;

    private GameSystem mGameSystem;

    private Random mRandom = new Random();

    public static GameListFragment newInstance(int ordinal) {
        GameListFragment fragment = new GameListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(GAME_LIST_SYSTEM, ordinal);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_list, container, false);

        init(view);

        return view;
    }

    private void init(View view) {

        mGameDownloadActivity = (GameDownloadActivity) getContext();

        Bundle bundle = getArguments();
        mGameSystem = GameSystem.values()[bundle.getInt(GAME_LIST_SYSTEM)];

        rvGameList = (RecyclerView) view.findViewById(R.id.fragment_game_list_list);
        mGameListAdapter = new GamesListAdapter(getContext());
        mGameListAdapter.setListSelector(new ListSelectorInterface() {
            @Override
            public void OnItemSelected(int position) {

                final Game game = mGameListAdapter.getItem(position);

                YesNoDialog.getConfirmDialog(getContext(),
                        getContext().getResources().getString(R.string.app_name),
                        String.format(getContext().getResources().getString(R.string.title_download_game), game.getTitle()),
                        getContext().getResources().getString(android.R.string.yes),
                        getContext().getResources().getString(android.R.string.no),
                        true,
                        new YesNoDialog.AlertDialogService() {
                            @Override
                            public void PositiveMethod(DialogInterface dialog, int id) {
                                urlConversion(game);
                            }

                            @Override
                            public void NegativeMethod(DialogInterface dialog, int id) {

                            }
                        }
                );
            }
        });

        mLayoutManager = new LinearLayoutManager(getContext());
        rvGameList.setLayoutManager(mLayoutManager);
        rvGameList.setAdapter(mGameListAdapter);

        new LoadPageTask().execute(mGameSystem.getUrlWithAlphabetListing('A'));
    }

    private class LoadPageTask extends AsyncTask<String, Void, ArrayList<Game>> {
        @Override
        protected ArrayList<Game> doInBackground(String... strings) {
            ArrayList<Game> gameList = new ArrayList<>();
            try {
                Document doc = Jsoup.connect(strings[0]).get();
                Element listOfGamesContent = doc.getElementById("list-of-games");
                Elements gameContent = listOfGamesContent.getElementsByTag("a");
                for (Element game : gameContent) {
                    Game gameObj = new Game(game.text(), game.attr("href"));
                    gameList.add(gameObj);
                }
            }
            catch (Exception e) { Log.e(LOG, "Error in getting html tags... " + e); }
            return gameList;
        }
        @Override
        protected void onPostExecute(ArrayList<Game> games) {
            mGameListAdapter.addItems(games);
        }
    }

    private void urlConversion(Game game) {
        switch (mGameSystem) {
            case SUPER_NINTENDO_ENTERTAINMENT_SYSTEM:
                try {
                    String ip = Constants.DOWNLOAD_IPS.get(getRandomNumber(0, Constants.DOWNLOAD_IPS.size() - 1));
                    String encodeFolder = URLEncoder.encode("Super Nintendo Roms", "utf-8");
                    String encodeTitle = URLEncoder.encode(game.getTitle(), "utf-8");
                    String url = String.format("http://%s/%s/may/120/%s/%s.zip", ip, Constants.DOWNLOAD_KEY, encodeFolder, encodeTitle).replace("+", "%20");;
                    initDownloadManager(game.getTitle(), url);
                } catch (Exception e) {
                    Log.e(LOG, "Error in parsing string to url encode... " + e);
                }
                break;

            case PLAYSTATION_2:

                break;
        }
    }

    public int getRandomNumber(int from, int to) { return mRandom.nextInt(to - from + 1) + from; }

    private void initDownloadManager(String title, String url) {
        Log.e(LOG, "Now downloading: " + url);
        Toast.makeText(getContext(), "Starting download for " + title, Toast.LENGTH_LONG).show();

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(String.format(getContext().getResources().getString(R.string.title_download_game), title));
        request.setTitle(title);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title + ".7z");

        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
}
