package com.trienoir.emuparadiser.models;

/**
 * Created by mClinica - Chad on 10/4/2017.
 */

public class Game {

    private long id;
    private String title;
    private String linkReference;

    public Game(String title, String linkReference) {
        this.id = Integer.parseInt(linkReference.substring(linkReference.lastIndexOf('/') + 1,
                linkReference.length()));
        this.title = title;
        this.linkReference = linkReference;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkReference() {
        return linkReference;
    }

    public void setLinkReference(String linkReference) {
        this.linkReference = linkReference;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", linkReference='" + linkReference + '\'' +
                '}';
    }
}
