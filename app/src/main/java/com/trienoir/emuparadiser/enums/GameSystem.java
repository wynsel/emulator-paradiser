package com.trienoir.emuparadiser.enums;

import com.trienoir.emuparadiser.utils.Constants;

import java.util.Locale;

/**
 * Created by mClinica - Chad on 10/4/2017.
 */

public enum GameSystem {

    SUPER_NINTENDO_ENTERTAINMENT_SYSTEM(5, "Super_Nintendo_Entertainment_System_(SNES)_ROMs"),
    PLAYSTATION_2(41 ,"Sony_Playstation_2_ISOs");

    private int id;
    private String title;

    GameSystem(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return String.format(Locale.getDefault(), "%s/%s/%d", Constants.BASE_URL, title, id);
    }

    public String getUrlWithAlphabetListing(char a) {
        return String.format(Locale.getDefault(), "%s/%s/%s%s/%d",
                Constants.BASE_URL, title, Constants.ALPHABET_LISTING_START, a, id);
    }
}
