package com.trienoir.emuparadiser.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mClinica - Chad on 10/4/2017.
 */

public class Constants {

    //https://www.emuparadise.me/roms/roms.php?gid=152536

    public static final String BASE_URL = "https://www.emuparadise.me";
    public static final String NUMBER_LISTING = "Games-Starting-With-Numbers";
    public static final String ALPHABET_LISTING_START = "Games-Starting-With-";
    public static final List<Character> ALPHABET_LISTING = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

    public static final String LINK_REFERENCE_BUNDLE_KEY = "link_reference_key";
    public static final String GAME_SYSTEM_BUNDLE_KEY = "game_system_key";
    public static final String TITLE_BUNDLE_KEY = "title_key";


    public static final String DOWNLOAD_KEY = "happyxhJ1ACmlTrxJQpol71nBc";
    public static final List<String> DOWNLOAD_IPS = Arrays.asList(
            "50.7.161.122",
            "50.7.161.234",
            "50.7.161.74",
            "50.7.136.26",
            "198.16.64.50",
            "50.7.188.26",
            "50.7.92.186",
            "208.77.22.75",
            "75.126.5.157",
            "75.126.5.164",
            "75.126.73.49",
            "68.235.37.3",
            "75.126.73.27");
}
