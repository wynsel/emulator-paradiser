package com.trienoir.emuparadiser.utils;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class GenericListAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public int mAddCount = 0;

    private ArrayList<T> mItems = new ArrayList<>();

    public ArrayList<T> getItems() {
        return mItems;
    }

    public T getItem (int position){
        return mItems.get(position);
    }

    public ArrayList<Integer> mSections = new ArrayList<>();

    public void setItems(ArrayList<T> items) {
        this.mItems = items;
    }

    public boolean contains(T items) {
        return this.mItems.contains(items);
    }

    public int size() {
        return this.mItems.size();
    }

    public boolean isEmpty() {
        return this.mItems.isEmpty();
    }

    public void addItems(ArrayList<T> items) {
        this.mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void addItems(Set<T> items) {
        this.mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void addItem(T item) {
        this.mItems.add(item);
        notifyDataSetChanged();
    }

    public void addItem(int i, T item) {
        this.mItems.add(i, item);
        notifyDataSetChanged();
    }

    public void addItemsOnTop(ArrayList<T> items) {
        this.mItems.addAll(0, items);
        notifyDataSetChanged();
    }

    public void addItemOnTop(T item) {
        this.mItems.add(0, item);
        notifyDataSetChanged();
    }

    public void modifyItem(T t, int position) {
        this.mItems.set(position, t);
        notifyDataSetChanged();
    }

    public void refresh(ArrayList<T> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public void clear() {
        this.mItems.clear();
        notifyDataSetChanged();
    }

    public void addSection(int position) {
        addItem(position, null);
    }

    protected void fillItems(ArrayList<T> items) {
        this.mItems = items;
    }

    protected void fillItems(List<T> items) {
        this.mItems = (ArrayList<T>) items;
    }

    protected abstract RecyclerView.ViewHolder setOnCreateViewHolder(ViewGroup parent, int viewType);

    protected int setGetItemViewType(T item) {
        return -1;
    }

    protected abstract void setOnBindViewHolder(RecyclerView.ViewHolder holder, int position, T item);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return setOnCreateViewHolder(parent, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        try {
            T t = mItems.get(position);
            return setGetItemViewType(t);
        } catch (Exception e) {
            return position;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        T t = mItems.get(position);
        setOnBindViewHolder(holder, position, t);
    }

    @Override
    public int getItemCount() {
        return mItems.size() + mAddCount;
    }

    protected void addTotalCount(int addCount) {
        this.mAddCount = addCount;
    }
}
